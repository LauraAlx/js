import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor( private http:HttpClient) { }

  getFilm(){
    return this.http.get('https://ghibliapi.herokuapp.com/films');
  }
  getDetails(filmId:String){
    return this.http.get(`https://ghibliapi.herokuapp.com/films?id=${filmId}`);
  }

  getPeople(peopleId:String){
    return this.http.get(`https://ghibliapi.herokuapp.com/people/?pId=${peopleId}`)
  }
  
}
