import { Component, OnInit } from '@angular/core';
import { FilmService } from '../services/film.service';


@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {
  film: any=[];
  search:String;
  constructor(private films:FilmService,) {
    this.search="";
    this.films.getFilm().subscribe(res => {console.log(res);
    this.film= res;
    })
   }

  ngOnInit(): void {
    
  }

}
