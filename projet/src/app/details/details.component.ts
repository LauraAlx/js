import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { FilmService } from '../services/film.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  film$: Observable<any> | undefined
  filmiD:String;

  constructor(private films:FilmService, private route:ActivatedRoute) {
    this.filmiD= this.route.snapshot.params.filmiD;
    this.film$=this.films.getDetails(this.filmiD).pipe(tap((res) => console.log(res)));
   }
   display(){
     alert("Nouveauté à venir")
   }
  ngOnInit(): void {
  }

}
