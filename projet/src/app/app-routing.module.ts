import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { FilmsComponent } from './films/films.component';

const routes: Routes = [{path: '',component: FilmsComponent }, {path: 'details/:filmiD', component: DetailsComponent},];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
